from django.urls import path

from .views import FileUploaderViewSet

urlpatterns = [
    path('api/file/', FileUploaderViewSet.as_view()),
]
