import datetime
import logging
import pandas as pd
import openpyxl

# from openpyxl.worksheet import ReadOnlyWorksheet
from openpyxl.cell import MergedCell

from django.http.response import HttpResponseRedirect, HttpResponse

from rest_framework.decorators import api_view, action
from rest_framework.generics import ListCreateAPIView, GenericAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView


from positions.models import *
from users.models import *


def get_date_repr(date):
    if date is None:
        return None
    if isinstance(date, datetime.datetime):
        return date
    elif isinstance(date, str):
        return datetime.datetime.strptime(date, '%d.%m.%y')
    else:
        return None

def get_str(value):
    if isinstance(value, str):
        return value
    elif isinstance(value, float):
        return str(int(value))
    else:
        return value


class FileUploaderViewSet(APIView):

    queryset = Position.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = None

    def post(self, request):
        time = datetime.datetime.timestamp(datetime.datetime.now())

        elapsed = lambda: datetime.datetime.timestamp(datetime.datetime.now()) - time

        logging.warning([time, request.data.get('detail')])
        if self.request.user.is_authenticated:
            file = self.request.data.get('file')

            if not file:
                return Response(status=404, data={'detail': 'Файл отсутствует в запросе'})

            wb = openpyxl.open(file)

            ws = wb.active

            Category.objects.all().delete()
            Consumer.objects.all().delete()
            Color.objects.all().delete()
            Position.objects.all().delete()
            Supplier.objects.all().delete()
            PositionModel.objects.all().delete()
            DaysPositionRel.objects.all().delete()
            MonthPositionRel.objects.all().delete()

            for col in ws.iter_cols(min_col=2, max_col=2, min_row=4):

                Category.objects.bulk_create(
                    (
                        Category(title=i) for i in filter(lambda x: x, set(cell.value for cell in col))
                    ), ignore_conflicts=True
                )

            for col in ws.iter_cols(min_col=4, max_col=4, min_row=4):

                Consumer.objects.bulk_create(
                    (
                        Consumer(name=get_str(i)) for i in filter(lambda x: x, set(cell.value for cell in col))
                    ), ignore_conflicts=True
                )

            for col in ws.iter_cols(min_col=5, max_col=5, min_row=4):
                # elif n == 5:
                Color.objects.bulk_create(
                    (
                        Color(title=i) for i in filter(lambda x: x, set(cell.value for cell in col))
                    ), ignore_conflicts=True
                )

            for col in ws.iter_cols(min_col=6, max_col=6, min_row=4):

                PositionModel.objects.bulk_create(
                    (
                        PositionModel(title=i) for i in filter(lambda x: x, set(cell.value for cell in col))
                    ), ignore_conflicts=True
                )

            for col in ws.iter_cols(min_col=7, max_col=7, min_row=4):

                Supplier.objects.bulk_create(
                    (
                        PositionModel(title=i) for i in filter(lambda x: x, set(cell.value for cell in col))
                    ), ignore_conflicts=True
                )

            logging.warning(['named created', elapsed()])

            Position.objects.all().delete()

            Position.objects.bulk_create(
                (
                    Position(
                        nomenclature=get_str(row[0].value),
                        category=Category.objects.get(title=row[1].value) if Category.objects.filter(title=row[1].value).exists() else None,
                        spec_date=get_date_repr(row[2].value),
                        consumer=Consumer.objects.get(name=get_str(row[3].value)) if Consumer.objects.filter(name=get_str(row[3].value)).exists() else None,
                        color=Color.objects.get(title=row[4].value) if Color.objects.filter(title=row[4].value).exists() else None,
                        position_model=PositionModel.objects.get(title=row[5].value) if PositionModel.objects.filter(title=row[5].value).exists() else None,
                        supplier=Supplier.objects.get(title=row[6].value) if Supplier.objects.filter(title=row[6].value).exists() else None,
                        ms_vendor_code=get_str(row[7].value),
                        ms_barcode=get_str(row[8].value),
                        vb_barcode=get_str(row[9].value)
                    ) for row in filter(lambda x: x[0].value, ws.iter_rows(min_row=4))
                ), ignore_conflicts=True
            )

            logging.warning(['positions', elapsed()])

            months = months_dict()

            def get_int(i) -> int:
                return i if isinstance(i, int) else 0

            # OPT 1

            # EoF OPT1

            # OPT 2

            detail = request.data.get('detail', False)

            if detail in [True, 'true']:
                for start_col_number in range(16, ws.max_column, 4):

                    logging.warning([start_col_number / ws.max_column])

                    if isinstance(ws.cell(1,start_col_number).value, str):
                        month = months.get(ws.cell(1,start_col_number).value)
                        MonthPositionRel.objects.bulk_create((
                            MonthPositionRel(
                                month=month,
                                year=2019 if start_col_number in range(15+7*4+1) else 2020,
                                position=Position.objects.get(nomenclature=row[0].value),
                                # position=row[0].value,
                                ordered_total=get_int(row[start_col_number].value),
                                returned_total=get_int(row[start_col_number+2].value),
                                sold_total=get_int(row[start_col_number+3].value)
                            ) for row in filter(lambda x: x[0].value, ws.iter_rows(min_row=4))
                        ), ignore_conflicts=True)
                    elif isinstance(ws.cell(1,start_col_number).value, datetime.date):
                        DaysPositionRel.objects.bulk_create(
                            (
                                DaysPositionRel(
                                    month=ws.cell(1,start_col_number).value.month,
                                    year=ws.cell(1,start_col_number).value.year,
                                    day=ws.cell(1,start_col_number).value.day,
                                    position=Position.objects.get(nomenclature=row[0].value),
                                    # position__pk=row[0].value,
                                    ordered_total=get_int(row[start_col_number].value),
                                    returned_total=get_int(row[start_col_number+2].value),
                                    sold_total=get_int(row[start_col_number+3].value)
                                    ) for row in filter(lambda x: x[0].value, ws.iter_rows(min_row=4))
                            ), ignore_conflicts=True)



            return Response(status=201, data={'success': True, 'elapsed': elapsed()})
