create role importer password 'importer';
alter role importer login;
create database products_importer;
grant ALL on DATABASE products_importer TO importer;

