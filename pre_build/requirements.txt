django==3.1.1
psycopg2
djangorestframework==3.11.1
pillow
django-rest-knox
openpyxl==3.0.5
pandas==1.1.2
