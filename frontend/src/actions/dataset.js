import axios from 'axios';
import { stopSubmit } from 'redux-form';

import { tokenConfig } from './auth';

import {
    TABLE_DATA_SEND,
    TABLE_DATA_RECEIVED,
    TABLE_DATA_FAIL,
} from './types';

// uploadData
export function uploadSuccess({ data }) {
  return {
    type: 'UPLOAD_DOCUMENT_SUCCESS',
    data,
  };
}

export function uploadFail(error) {
  return {
    type: 'UPLOAD_DOCUMENT_FAIL',
    error,
  };
}

export const uploadDocumentRequest = ({ file, name, token, detail }) => async (dispatch, getState) => {
  let data = new FormData();

  data.append('file', file);
  data.append('name', name);
  data.append('detail', detail);

  try {
      const res = await axios.post('/api/file/', data, tokenConfig(getState));
      dispatch({
          type: TABLE_DATA_SEND,
          payload: res.data
      });
      return res;
  } catch (e) {
  }
};


export const getData = (page) => async (dispatch, getState) => {
  
  let config = tokenConfig(getState);

  config['params'] = {page: page};

  console.info([page, config])

  try {
      const res = await axios.get('/api/table', config);
      
      // // console.info(res);
      // dispatch({
      //     type: TABLE_DATA_RECEIVED,
      //     payload: res.data
      // });

      return res.data;
  } catch (e) {
      
  }
};
