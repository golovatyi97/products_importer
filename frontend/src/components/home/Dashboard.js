import React, { Component } from 'react';
import InfiniteTable from '../common/InfiniteTable';
import TableUploader from '../common/TableUploader';

class Dashboard extends Component {
  render() {
    return (
      <div className='ui container'>
        <TableUploader style='width: 100vw; height: 10vh' />
        <InfiniteTable style='width: 100vw' />
      </div>
    );
  }
}

export default Dashboard;
