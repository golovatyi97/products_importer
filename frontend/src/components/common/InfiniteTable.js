// import 'react-virtualized/styles.css';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Table, Column, AutoSizer, InfiniteLoader } from 'react-virtualized';
import { getData } from '../../actions/dataset';
import InfiniteScroll from 'react-infinite-scroll-component';
import store from '../../store';

const style = {
   height: 30,
   border: "1px solid green",
   margin: 6,
   padding: 8
 };

class InfiniteTable extends React.Component {
   constructor(props) {
      super(props);
      this.state ={
         file:null,
         token: props.token,
         loading: false,
         items: [],
         is_finished: false,
         page: 1
       };
      this.fetchMoreData = this.fetchMoreData.bind(this);
      //  data

      this.fetchMoreData();
   }

   fetchMoreData () {
      console.info(['loadMore', this.state.page]);

      store.dispatch(getData(this.state.page)).then(
         (res) => {
            if (res) {
               if (res.results) {

                  this.setState({
                     items: this.state.items.concat(...res.results)
                  });
                  this.state.items = this.state.items.concat(res.results);
                  console.info([res.results, this.state.page, this.state]);
                  this.state.page = this.state.page + 1;


                  console.info(["result", this.state.items]);
               }
            }
         }
      ).catch(
         (_) => {
            this.setState({is_finished: true});
         }
      );

      return   setTimeout(() => {
       }, 1);;
   }

   render () {
      return (
         <div>
         <h1>Товары</h1>
         <hr />
         <InfiniteScroll
           dataLength={this.state.items.length}
           next={this.fetchMoreData}
           hasMore={!this.state.is_finished}
           loader={<h4>Загрузка...</h4>}
           height={400}
           endMessage={
             <p style={{ textAlign: "center" }}>
               <b>Yay! You have seen it all</b>
             </p>
           }
         >
            <table>
               <thead>
                  <th>Номенклатура</th>
                  <th>Категория</th>
                  <th>Спецификация</th>
                  <th>Контрагент</th>
                  <th>ЦВ</th>
                  <th>Модель</th>
                  <th>Поставщик</th>
                  <th>Артикул МС</th>
                  <th>Баркод МС</th>
                  <th>Баркод ВБ</th>
               </thead>
               <tbody>
               {this.state.items.map((i, index) => (
                  <tr style={style} key={index}>
                     <td>{i.nomenclature}</td>
                     <td>{i.category}</td>
                     <td>{i.spec_date}</td>
                     <td>{i.consumer}</td>
                     <td>{i.color}</td>
                     <td>{i.position_model}</td>
                     <td>{i.supplier}</td>
                     <td>{i.ms_vendor_code}</td>
                     <td>{i.ms_barcode}</td>
                     <td>{i.vb_barcode}</td>
                  </tr>
               ))}       
               </tbody>
            </table>
         
         </InfiniteScroll>
       </div>
    )
  }
}

const mapStateToProps = state => ({
   //  isAuthenticated: state.auth.isAuthenticated
       token: state.auth.token,
      //  items: state.results
   });
   //
   //
   InfiniteTable = connect(
     mapStateToProps,
     { getData }
   )(InfiniteTable);

export default InfiniteTable;
