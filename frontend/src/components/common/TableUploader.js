import React from 'react'
import axios, { post } from 'axios';
import store from '../../store';
import { connect } from 'react-redux';
import { uploadDocumentRequest } from '../../actions/dataset';

class TableUploader extends React.Component {

  constructor(props) {
    super(props);
    this.state ={
      file:null,
      token: props.token,
      loading: false
    };
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
    this.uploadWithDates = this.uploadWithDates.bind(this);
//    this.fileUpload = this.fileUpload.bind(this)
  }
  onFormSubmit(e){
    e.preventDefault() // Stop form submit
    console.info([this.state.file, this.props]);
    const file = this.state.file;
    this.setState({loading: true});
    this.props.uploadDocumentRequest(
        {file, name: 'file.xlsx', token: this.props.token
        }).then((res)=>{
          this.setState({loading: false});
          alert("Успешно загружено. Время загрузки "+res.data.elapsed+" c.");
    });
  }

  uploadWithDates(e) {
    const file = this.state.file;
    this.setState({loading: true});
    this.props.uploadDocumentRequest(
        {file, name: 'file.xlsx', token: this.props.token, detail: 'true'
        }).then((res)=>{
          this.setState({loading: false});
          alert("Успешно загружено. Время загрузки "+res.data.elapsed+" c.");
    });
  }

  onChange(e) {
    this.setState({file:e.target.files[0]})
  }

  render() {

    const loading = this.state.loading;

    return (

    <div>

        {
            !loading  &&
              <form onSubmit={this.onFormSubmit}>
                <h1>Загрузить XLSX </h1>
                <input type="file" onChange={this.onChange} />
                <button type="submit">Загрузить без детализации дат</button>
                <button onClick={this.uploadWithDates}>Загрузить с детализацией дат</button>
              </form>
        }

        {
            loading  &&
              <div>
                <span style={{zIndex: 3}}>Идет обработка запроса</span>
                <div style={{height: 100+'%', width: 100+'%', opacity: 0.5, zIndex: 2}}></div>
              </div>  
        }
    </div>
   )
  }
}

const mapStateToProps = state => ({
//  isAuthenticated: state.auth.isAuthenticated
    token: state.auth.token
});
//
//
TableUploader = connect(
  mapStateToProps,
  { uploadDocumentRequest }
)(TableUploader);

export default TableUploader;
