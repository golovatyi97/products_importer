# Generated by Django 3.1.1 on 2020-09-26 15:51

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_supplier'),
        ('positions', '0003_auto_20200926_1520'),
    ]

    operations = [
        migrations.AddField(
            model_name='position',
            name='consumer',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='users.consumer', verbose_name='Контрагент'),
        ),
        migrations.AlterField(
            model_name='position',
            name='supplier',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='users.supplier', verbose_name='Поставщик'),
        ),
    ]
