import logging

from django.core.handlers import base, exception

from rest_framework import serializers, fields
from .models import *


class PositionWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Position
        fields = '__all__'

class PositionReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Position
        fields = '__all__'

    category = serializers.SerializerMethodField()
    supplier = serializers.SerializerMethodField()
    consumer = serializers.SerializerMethodField()
    color = serializers.SerializerMethodField()
    position_model = serializers.SerializerMethodField()

    monthly_statistics = serializers.SerializerMethodField()
    days_statistics = serializers.SerializerMethodField()

    #
    def get_category(self, obj):
        return obj.category.title if obj.category else None

    def get_supplier(self, obj):
        return obj.supplier.title if obj.supplier else None

    def get_consumer(self, obj):
        return obj.consumer.name if obj.consumer else None

    def get_color(self, obj):
        return obj.color.title if obj.color else None

    def get_position_model(self, obj):
        return obj.position_model.title if obj.position_model else None

    def get_monthly_statistics(self, obj):

        logging.warning([
            (i.ordered_total, i.returned_total, i.sold_total) for i in obj.month_statistics.all().order_by('year', 'month')
        ])

        return MonthSerializer(obj.month_statistics.all().order_by('year', 'month'), many=True).data

    def get_days_statistics(self, obj):
        return DaysSerializer(obj.daily_statistics.all().order_by('year', 'month'), many=True).data


class MonthSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = MonthPositionRel

class DaysSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = DaysPositionRel
