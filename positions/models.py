import datetime
import logging

from django.db import models

# 1st opt (w dividing titled-only models, such as colors etc)

def months_dict() -> dict:
    return dict(
        zip(
            ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            range(1,13)
        )
    )

def month_choices() -> list:
    # months = [[i, datetime.date(2008, i, 1).strftime('%B')] for i in range(1,13)]

    months = list(
        zip(
            range(1,13),
            ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']
        )
    )

    months.append([0, 'месяц не заведен'])
    return months


class Position(models.Model):
    nomenclature = models.IntegerField(verbose_name='Номенклатура', blank=True, null=False, unique=True, primary_key=True)
    category = models.ForeignKey(verbose_name='Категория',to='positions.Category', on_delete=models.SET_NULL, blank=True, null=True)
    spec_date = models.DateField(verbose_name='Спецификиция', null=True, blank=True)
    consumer = models.ForeignKey(verbose_name='Контрагент', to='users.Consumer', on_delete=models.SET_NULL, blank=True, null=True)
    supplier = models.ForeignKey(verbose_name='Поставщик', to='users.Supplier', on_delete=models.SET_NULL, blank=True, null=True)
    color = models.ForeignKey(verbose_name='Цвет', to='positions.Color', on_delete=models.SET_NULL, null=True, blank=True)
    position_model = models.ForeignKey(verbose_name='Модель', to='positions.PositionModel', on_delete=models.SET_NULL, null=True, blank=True)

    ms_vendor_code = models.CharField(verbose_name='Артикул МС', max_length=100, blank=True, null=True)
    ms_barcode = models.CharField(verbose_name='Баркод МС', max_length=100, blank=True, null=True)
    vb_barcode = models.CharField(verbose_name='Баркод ВБ', max_length=100, blank=True, null=True)

    class Meta:
        verbose_name = 'Позиция товара'
        verbose_name_plural = 'Позиции товаров'
        # indexes = []
        # order = 'nomenclature'



class MonthPositionRel(models.Model):
    month = models.SmallIntegerField(verbose_name='мес.', choices=month_choices(), blank=True, default=0)
    year = models.SmallIntegerField(verbose_name='г.',  blank=True, default=0)
    position = models.ForeignKey(Position, related_name='month_statistics', on_delete=models.CASCADE, blank=True)

    ordered_total = models.IntegerField(verbose_name='Заказано', blank=True, default=0)
    returned_total = models.IntegerField(verbose_name='Возвраты', blank=True, default=0)
    sold_total = models.IntegerField(verbose_name='Продано', blank=True, default=0)

    # Todo: остатки: клиент или сервер, либо хранимый рассчет по аналогии с odoo

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['month', 'year', 'position'], name='unique_month_for_position'),
        ]
        verbose_name = 'Результаты продаж за месяц'
        verbose_name_plural = 'Результаты продаж за месяц'
        # indexes = []

    # def __init__(self, *args, **kwargs):
    #     super(MonthPositionRel, self).__init__(args, kwargs)
    #
    #     if self.pk:
    #         if self.month == datetime.date.today().month:
    #             if DaysPositionRel.objects.filter(month=self.month, year=self.year).exists():
    #                 days = DaysPositionRel.objects.filter(month=self.month, year=self.year).aggregate(
    #                     ordered=models.Sum('ordered_total'), returned=models.Sum('returned_total'),
    #                     sold=models.Sum('sold_total')
    #                 )
    #                 logging.warning(['gfddfhgf', days])
    #
    #
    #                 self.ordered_total = days.get('ordered')
    #                 self.returned_total = days.get('returned')
    #                 self.sold_total = days.get('sold')
    #                 self.save()


class DaysPositionRel(models.Model):
    month = models.SmallIntegerField(verbose_name='мес.', choices=month_choices(),
                                     blank=True, default=0)
    year = models.SmallIntegerField(verbose_name='г.',  blank=True, default=0)
    day = models.SmallIntegerField(verbose_name='д.',  blank=True, default=0)
    position = models.ForeignKey(Position, related_name='daily_statistics', on_delete=models.CASCADE, blank=True)

    ordered_total = models.IntegerField(verbose_name='Заказано', blank=True, default=0)
    returned_total = models.IntegerField(verbose_name='Возвраты', blank=True, default=0)
    sold_total = models.IntegerField(verbose_name='Продано', blank=True, default=0)

    def save(self, *args, **kwargs):
        to_python = lambda x: models.SmallIntegerField().to_python(x)
        try:
            datetime.datetime(to_python(self.year), to_python(self.month), to_python(self.day))
            super(DaysPositionRel, self).save(*args, **kwargs)
        except ValueError:
            raise Exception('Date is incorrect')

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['month', 'year', 'day', 'position'], name='unique_day_for_position'),
        ]
        verbose_name = 'Результаты продаж за месяц'
        verbose_name_plural = 'Результаты продаж за месяц'
        # indexes = []


class Category(models.Model):
    title = models.CharField(verbose_name='Наименование', max_length=100, unique=True)
    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

class Color(models.Model):
    title = models.CharField(verbose_name='Наименование', max_length=100, unique=True)
    class Meta:
        verbose_name = 'Цвет'
        verbose_name_plural = 'Цвета'

class PositionModel(models.Model):
    title = models.CharField(verbose_name='Наименование', max_length=100, unique=True)
    class Meta:
        verbose_name = 'Модель'
        verbose_name_plural = 'Модели'
