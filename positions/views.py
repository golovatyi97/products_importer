import logging

from django.views.generic.list import MultipleObjectMixin, ListView

from rest_framework.response import Response

from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.viewsets import ModelViewSet, ViewSet

from positions.models import *
from positions.serializers import *


class PositionsViewSet(ModelViewSet):

    queryset = Position.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = PositionReadSerializer
    pagination_class = PageNumberPagination

    def get_serializer_class(self):
        return PositionReadSerializer

    def get_queryset(self):
        logging.warning(self.request)
        return self.queryset
    #
    # def list(self, request, *args, **kwargs):
    #
    #     logging.warning(self.request.query_params)
    #
    #     return Response(status=200, data={})

