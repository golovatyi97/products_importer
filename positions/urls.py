from django.urls import path

from .views import PositionsViewSet

urlpatterns = [
    path('api/table', PositionsViewSet.as_view({'get': 'list'}), name='table'),
]
