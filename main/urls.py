"""
URL Configuration
"""
from django.contrib import admin
from django.urls import path, include

from django.conf.urls import include, url
from django.views.generic import TemplateView

urlpatterns = [
    path('', include('file_uploader.urls')),
    path('', include('frontend.urls')),
    path('', include('positions.urls')),
    path('admin/', admin.site.urls),
    path('api/auth/', include('users.urls')),
]

urlpatterns += [url(r'^', TemplateView.as_view(template_name='frontend/index.html'))]
