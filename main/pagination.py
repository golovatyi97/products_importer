from rest_framework.pagination import PageNumberPagination


class DynamicPagination(PageNumberPagination):
    page_size_query_param = 'page_size'

    page_size = 100

    def get_page_size(self, request):
        if request.query_params.get('paginate', None) is None:
            return None

        return int(request.query_params.get(self.page_size_query_param, 100))
